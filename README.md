# MulticastCluster
Very simple distributed cluster
What it can do:

1. You can start multiple nodes
2. Each node will find other nodes through multicast
3. Nodes will decide who is the leader
4. Follower nodes will connect to the leader node through TCP
5. Leader node will ping follower nodes
6. Follower nodes will answer leader

Libraries used:
1. Netty for client / server capabilities
2. Log4J2 for logging
3. JUnit for an integration test

# How to build and launch
## Requirements:
1. Java 17
   
## Build and launch steps:
1. `git clone https://gitlab.com/Xakep_SDK/multicastcluster.git`
2. `cd multicastcluster`
3. `mvn package`
4. `cd target`
5. `java -jar MulticastCluster-*.jar`
6. You can launch as many copies as you want