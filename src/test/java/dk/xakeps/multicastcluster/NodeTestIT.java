package dk.xakeps.multicastcluster;

import org.junit.jupiter.api.Test;

import java.net.*;
import java.time.Duration;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

public class NodeTestIT {
    @Test
    public void test() throws Exception {
        Config firstNodeConfig = createConfig();
        Node firstNode = new Node(firstNodeConfig);
        firstNode.start();

        Config secondNodeConfig = createConfig();
        Node secondNode = new Node(secondNodeConfig);
        secondNode.start();

        Config thirdNodeConfig = createConfig();
        Node thirdNode = new Node(thirdNodeConfig);
        thirdNode.start();

        TimeUnit.SECONDS.sleep(10);
        assertTrue(firstNodeConfig.getClientManager().isAcceptClients());
        assertFalse(secondNodeConfig.getClientManager().isAcceptClients());
        assertFalse(thirdNodeConfig.getClientManager().isAcceptClients());

        firstNode.stop();
        TimeUnit.SECONDS.sleep(10);
        assertTrue(secondNodeConfig.getClientManager().isAcceptClients());
        assertFalse(thirdNodeConfig.getClientManager().isAcceptClients());

        firstNode.start();
        TimeUnit.SECONDS.sleep(10);
        assertTrue(firstNodeConfig.getClientManager().isAcceptClients());
        assertFalse(secondNodeConfig.getClientManager().isAcceptClients());
        assertFalse(thirdNodeConfig.getClientManager().isAcceptClients());

        firstNode.stop();
        secondNode.stop();
        thirdNode.stop();
    }

    private Config createConfig() throws SocketException, UnknownHostException {
        InetAddress tcpAddress = InetAddress.getByName("127.0.01");
        int tcpPort = ThreadLocalRandom.current().nextInt(1025, 65536);
        InetSocketAddress tcpServerAddress = new InetSocketAddress(tcpAddress, tcpPort);

        InetAddress groupAddr = InetAddress.getByName("239.0.0.0");
        int groupPort = 1234;
        InetSocketAddress groupSocketAddress = new InetSocketAddress(groupAddr, groupPort);

        NetworkInterface multicastInterface = NetworkInterface.getByInetAddress(tcpServerAddress.getAddress());

        ClientManager clientManager = new ClientManager();
        Duration pingRate = Duration.ofSeconds(5);
        Duration initialWaitTime = Duration.ofSeconds(5);

        return new Config(
                pingRate,
                initialWaitTime,
                System.currentTimeMillis(),
                tcpServerAddress,
                groupSocketAddress,
                multicastInterface,
                clientManager
        );
    }
}