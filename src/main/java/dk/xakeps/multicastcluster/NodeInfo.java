package dk.xakeps.multicastcluster;

import java.net.InetSocketAddress;
import java.util.Arrays;

/**
 * Represents basic information about each node
 * Combination of a timestamp and node TCP server address represent unique node ID
 */
public interface NodeInfo extends Comparable<NodeInfo> {
    /**
     * @return node start time, serves as a node id
     */
    long timestamp();

    /**
     * @return node socket address
     */
    InetSocketAddress address();

    /**
     * Older nodes first (smaller timestamp)
     * Nodes with equal timestamp are compared by IP
     * Nodes with equal IPs are compared by port, smaller port is first
     * @param o another node
     * @return comparison result
     */
    @Override
    default int compareTo(NodeInfo o) {
        int compare = Long.compare(timestamp(), o.timestamp());
        if (compare != 0) {
            return compare;
        }

        byte[] o1 = address().getAddress().getAddress();
        byte[] o2 = o.address().getAddress().getAddress();
        compare = Arrays.compare(o1, o2);
        if (compare != 0) {
            return compare;
        }

        return Integer.compare(address().getPort(), o.address().getPort());
    }
}
