package dk.xakeps.multicastcluster;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.util.List;
import java.util.Objects;

/**
 * Netty pipeline packet decoder
 */
public class PacketDecoder extends ByteToMessageDecoder {
    private final PacketRegistry packetRegistry;

    public PacketDecoder(PacketRegistry packetRegistry) {
        this.packetRegistry = Objects.requireNonNull(packetRegistry, "packetRegistry");
    }

    @Override
    protected void decode(ChannelHandlerContext channelHandlerContext, ByteBuf byteBuf, List<Object> list) {
        list.add(packetRegistry.read(byteBuf));
    }
}
