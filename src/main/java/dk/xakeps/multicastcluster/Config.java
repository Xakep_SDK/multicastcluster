package dk.xakeps.multicastcluster;

import dk.xakeps.multicastcluster.multicast.packets.LeaderInfoPacket;
import dk.xakeps.multicastcluster.multicast.packets.VoteRequestPacket;
import dk.xakeps.multicastcluster.multicast.packets.VoteResponsePacket;

import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.time.Duration;

/**
 * Contains static configuration
 * or shares singleton beans
 */
public class Config {
    private final Duration pingRate;
    private final Duration initialWaitTime;

    private final long timestamp;
    private final InetSocketAddress tcpServerAddress;
    private final InetSocketAddress groupAddress;
    private final NetworkInterface multicastInterface;
    private final ClientManager clientManager;

    private final VoteRequestPacket voteRequestPacket;
    private final VoteResponsePacket voteResponsePacket;
    private final LeaderInfoPacket leaderInfoPacket;

    public Config(Duration pingRate,
                  Duration initialWaitTime,
                  long timestamp,
                  InetSocketAddress tcpServerAddress,
                  InetSocketAddress groupAddress,
                  NetworkInterface multicastInterface,
                  ClientManager clientManager) {
        this.pingRate = pingRate;
        this.initialWaitTime = initialWaitTime;
        this.timestamp = timestamp;
        this.tcpServerAddress = tcpServerAddress;
        this.groupAddress = groupAddress;
        this.multicastInterface = multicastInterface;
        this.clientManager = clientManager;

        this.voteRequestPacket = new VoteRequestPacket(timestamp);
        this.voteResponsePacket = new VoteResponsePacket(timestamp, tcpServerAddress);
        this.leaderInfoPacket = new LeaderInfoPacket(timestamp, tcpServerAddress);
    }

    public Duration getPingRate() {
        return pingRate;
    }

    public Duration getInitialWaitTime() {
        return initialWaitTime;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public InetSocketAddress getTcpServerAddress() {
        return tcpServerAddress;
    }

    public InetSocketAddress getGroupAddress() {
        return groupAddress;
    }

    public NetworkInterface getMulticastInterface() {
        return multicastInterface;
    }

    public ClientManager getClientManager() {
        return clientManager;
    }

    public VoteRequestPacket getVoteRequestPacket() {
        return voteRequestPacket;
    }

    public VoteResponsePacket getVoteResponsePacket() {
        return voteResponsePacket;
    }

    public LeaderInfoPacket getLeaderInfoPacket() {
        return leaderInfoPacket;
    }
}
