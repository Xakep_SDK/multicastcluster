package dk.xakeps.multicastcluster.tcp;

import dk.xakeps.multicastcluster.PacketHandler;
import dk.xakeps.multicastcluster.tcp.packets.PingPacket;
import dk.xakeps.multicastcluster.tcp.packets.PongPacket;
import io.netty.channel.ChannelHandlerContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Represents basic implementation for TCP client and server packet handling
 */
public interface TcpPacketHandler extends PacketHandler {
    /*private*/ Logger LOGGER = LogManager.getLogger(TcpPacketHandler.class);

    /**
     * Called when ping packet received
     * @param ctx context
     * @param packet ping packet
     */
    default void handle(ChannelHandlerContext ctx, PingPacket packet) {
        LOGGER.trace("Ping from: {}", ctx.channel().remoteAddress());
        ctx.writeAndFlush(new PongPacket(packet.pingTime()));
    }

    /**
     * Called when ping packet response (Pong) received
     * @param ctx context
     * @param packet pong packet
     */
    default void handle(ChannelHandlerContext ctx, PongPacket packet) {
        LOGGER.trace("Pong from: {}, time: {}", ctx.channel().remoteAddress(),
                (System.currentTimeMillis() - packet.pingTime()));
    }

    /**
     * Called when server disconnected or client disconnected
     * @param ctx context
     */
    void onDisconnect(ChannelHandlerContext ctx);
}
