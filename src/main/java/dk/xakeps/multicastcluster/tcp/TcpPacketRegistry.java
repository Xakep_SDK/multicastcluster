package dk.xakeps.multicastcluster.tcp;

import dk.xakeps.multicastcluster.PacketRegistry;
import dk.xakeps.multicastcluster.tcp.packets.PingPacket;
import dk.xakeps.multicastcluster.tcp.packets.PongPacket;

/**
 * Contains all TCP packets
 */
public class TcpPacketRegistry extends PacketRegistry {
    public static final PacketRegistry INSTANCE;

    static {
        TcpPacketRegistry registry = new TcpPacketRegistry();

        registry.registerPacket(PingPacket.class, buf -> new PingPacket(buf.readLong()));
        registry.registerPacket(PongPacket.class, buf -> new PongPacket(buf.readLong()));

        INSTANCE = registry;
    }
}
