package dk.xakeps.multicastcluster.tcp;

import dk.xakeps.multicastcluster.*;
import dk.xakeps.multicastcluster.tcp.packets.TcpClientPacketHandler;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.LengthFieldPrepender;
import io.netty.handler.logging.LoggingHandler;

import java.net.InetSocketAddress;

/**
 * Follower client, connects to the TCP Leader server
 */
public class TcpClient {
    private final Bootstrap bootstrap;

    private Channel channel;

    public TcpClient() {
        this.bootstrap = createBootstrap();
    }

    /**
     * Disconnects from the server if connected
     */
    public void disconnect() {
        if (channel != null) {
            channel.disconnect();
        }
    }

    /**
     * Connects to the server
     * @param tcpServerAddress server to connect
     * @param closeListener action to run when this client is disconnected
     */
    public void connect(InetSocketAddress tcpServerAddress, Runnable closeListener) {
        disconnect();
        this.channel = bootstrap.connect(tcpServerAddress).channel();
        channel.closeFuture().addListener(future -> {
            closeListener.run();
        });
    }

    /**
     * @return true if this client is connected to the server
     */
    public boolean isConnected() {
        return channel != null && channel.isActive();
    }

    private static Bootstrap createBootstrap() {
        EventLoopGroup workerGroup = new NioEventLoopGroup();
        return new Bootstrap()
                .group(workerGroup)
                .channel(NioSocketChannel.class)
                .option(ChannelOption.TCP_NODELAY, true)
                .handler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel ch) {
                        ChannelPipeline pipeline = ch.pipeline();

                        pipeline
                                .addLast(new PacketDecoder(TcpPacketRegistry.INSTANCE))
                                .addLast(new LengthFieldBasedFrameDecoder(1024, 0, 4))

                                .addLast(new PacketEncoder(TcpPacketRegistry.INSTANCE))
                                .addLast(new LengthFieldPrepender(4))
                                .addLast(new TcpHandler(new TcpClientPacketHandler()));
                    }
                });
    }
}
