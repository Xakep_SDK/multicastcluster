package dk.xakeps.multicastcluster.tcp;

import dk.xakeps.multicastcluster.*;
import dk.xakeps.multicastcluster.tcp.packets.TcpServerPacketHandler;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.LengthFieldPrepender;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.InetSocketAddress;
import java.util.Objects;

/**
 * Represents TCP leader server
 */
public class TcpServer {
    private static final Logger LOGGER = LogManager.getLogger(TcpServer.class);
    private final ServerBootstrap bootstrap;
    private final Config config;
    private Channel channel;

    public TcpServer(Config config) {
        this.config = Objects.requireNonNull(config, "config");
        this.bootstrap = createBoostrap(config);
    }

    /**
     * Starts this TCP server
     */
    public void start() {
        LOGGER.info("TCP Server is running: {}", config.getTcpServerAddress());
        this.channel = bootstrap.bind(config.getTcpServerAddress()).channel();
    }

    /**
     * Stops this server if it was started
     */
    public void stop() throws InterruptedException {
        if (this.channel == null) {
            return;
        }
        LOGGER.info("TCP Server stopped");
        this.channel.close().sync();
        this.channel = null;
    }

    private static ServerBootstrap createBoostrap(Config config) {
        EventLoopGroup bossGroup = new NioEventLoopGroup();
        EventLoopGroup workerGroup = new NioEventLoopGroup();

        return new ServerBootstrap()
                .group(bossGroup, workerGroup)
                .channel(NioServerSocketChannel.class)
                .childHandler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel ch) {
                        ChannelPipeline pipeline = ch.pipeline();
                        pipeline
                                .addLast(new PacketDecoder(TcpPacketRegistry.INSTANCE))
                                .addLast(new LengthFieldBasedFrameDecoder(256, 0, 4))

                                .addLast(new PacketEncoder(TcpPacketRegistry.INSTANCE))
                                .addLast(new LengthFieldPrepender(4))
                                .addLast(new TcpHandler(new TcpServerPacketHandler(config)));
                    }
                })
                .childOption(ChannelOption.SO_KEEPALIVE, true);
    }
}