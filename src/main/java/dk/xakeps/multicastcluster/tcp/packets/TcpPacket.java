package dk.xakeps.multicastcluster.tcp.packets;

import dk.xakeps.multicastcluster.PacketHandler;
import dk.xakeps.multicastcluster.packets.Packet;
import dk.xakeps.multicastcluster.tcp.TcpPacketHandler;
import io.netty.channel.ChannelHandlerContext;

/**
 * Represents TCP packet
 */
public interface TcpPacket extends Packet {
    @Override
    default void handle(ChannelHandlerContext ctx, PacketHandler packetHandler) {
        handle(ctx, (TcpPacketHandler) packetHandler);
    }

    void handle(ChannelHandlerContext ctx, TcpPacketHandler packetHandler);
}
