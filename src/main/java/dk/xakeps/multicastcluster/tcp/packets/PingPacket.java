package dk.xakeps.multicastcluster.tcp.packets;

import dk.xakeps.multicastcluster.tcp.TcpPacketHandler;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;

/**
 * Ping packet, client should response with pong packet with pingTime from this packet
 */
public record PingPacket(long pingTime) implements TcpPacket {
    @Override
    public void write(ByteBuf buf) {
        buf.writeLong(pingTime);
    }

    @Override
    public void handle(ChannelHandlerContext ctx, TcpPacketHandler packetHandler) {
        packetHandler.handle(ctx, this);
    }
}
