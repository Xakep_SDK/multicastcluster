package dk.xakeps.multicastcluster.tcp.packets;

import dk.xakeps.multicastcluster.tcp.TcpPacketHandler;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;

/**
 * Pong packet, is a response to the ping packet, contains pingTime from the pingPacket
 */
public record PongPacket(long pingTime) implements TcpPacket {
    @Override
    public void write(ByteBuf buf) {
        buf.writeLong(pingTime);
    }

    @Override
    public void handle(ChannelHandlerContext ctx, TcpPacketHandler packetHandler) {
        packetHandler.handle(ctx, this);
    }
}
