package dk.xakeps.multicastcluster.tcp.packets;

import dk.xakeps.multicastcluster.tcp.TcpPacketHandler;
import io.netty.channel.ChannelHandlerContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Handles TCP packets as a client
 */
public class TcpClientPacketHandler implements TcpPacketHandler {
    private static final Logger LOGGER = LogManager.getLogger(TcpServerPacketHandler.class);

    @Override
    public void onConnect(ChannelHandlerContext ctx) {
        LOGGER.trace("Connected to the server: {}", ctx.channel().remoteAddress());
    }

    @Override
    public void onDisconnect(ChannelHandlerContext ctx) {
        LOGGER.trace("Disconnected from the server: {}", ctx.channel().remoteAddress());
    }
}
