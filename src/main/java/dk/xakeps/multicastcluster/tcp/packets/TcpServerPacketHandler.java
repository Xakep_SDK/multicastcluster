package dk.xakeps.multicastcluster.tcp.packets;

import dk.xakeps.multicastcluster.Config;
import dk.xakeps.multicastcluster.tcp.TcpPacketHandler;
import io.netty.channel.ChannelHandlerContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * Handles TCP packets as a server
 */
public class TcpServerPacketHandler implements TcpPacketHandler {
    private static final Logger LOGGER = LogManager.getLogger(TcpServerPacketHandler.class);

    private final Config config;

    public TcpServerPacketHandler(Config config) {
        this.config = Objects.requireNonNull(config, "config");
    }

    @Override
    public void onConnect(ChannelHandlerContext ctx) {
        if (!config.getClientManager().isAcceptClients()) {
            ctx.close();
            return;
        }
        LOGGER.trace("Client connected: {}", ctx.channel().remoteAddress());
        config.getClientManager().addClient(ctx.channel());
        ctx.executor().scheduleAtFixedRate(() -> {
            ctx.writeAndFlush(new PingPacket(System.currentTimeMillis()));
        }, 0, config.getPingRate().toMillis(), TimeUnit.MILLISECONDS);
    }

    @Override
    public void onDisconnect(ChannelHandlerContext ctx) {
        LOGGER.trace("Client disconnected: {}", ctx.channel().remoteAddress());
    }
}
