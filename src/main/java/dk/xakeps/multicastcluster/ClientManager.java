package dk.xakeps.multicastcluster;

import io.netty.channel.Channel;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.util.concurrent.GlobalEventExecutor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Manages client connections to the server
 */
public class ClientManager {
    private static final Logger LOGGER = LogManager.getLogger(ClientManager.class);
    private final ChannelGroup channels = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);

    private volatile boolean acceptClients;

    /**
     * @return clients count
     */
    public int clientsCount() {
        return channels.size();
    }

    /**
     * Adds client to this client manager
     * Client will be disconnected if this client manager can't accept clients
     * @param channel client to add
     */
    public void addClient(Channel channel) {
        if (acceptClients) {
            channels.add(channel);
        } else {
            LOGGER.warn("Client tried to connect, but I can't accept clients: {}", channel.remoteAddress());
            channel.close();
        }
    }

    /**
     * Disconnects all clients
     */
    public void disconnectAll() {
        channels.close();
    }

    /**
     * @return true if this ClientManager can accept clients
     */
    public boolean isAcceptClients() {
        return acceptClients;
    }

    /**
     * Sets this ClientManager state
     * @param acceptClients whether this ClientManager can accept clients
     */
    public void setAcceptClients(boolean acceptClients) {
        this.acceptClients = acceptClients;
    }
}
