package dk.xakeps.multicastcluster;

import dk.xakeps.multicastcluster.multicast.MulticastServer;
import dk.xakeps.multicastcluster.tcp.TcpServer;

public class Node {
    private final TcpServer tcpServer;
    private final MulticastServer multicastServer;

    public Node(Config config) {
        this.tcpServer = new TcpServer(config);
        this.multicastServer = new MulticastServer(config);
    }

    public void start() throws Exception {
        tcpServer.start();
        multicastServer.start();
    }

    public void stop() throws InterruptedException {
        tcpServer.stop();
        multicastServer.stop();
    }
}
