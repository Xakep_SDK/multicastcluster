package dk.xakeps.multicastcluster;

import java.util.Objects;

public final class Utils {
    private Utils() {
    }

    /**
     * Compares nodes and finds true leader
     * @param left left node
     * @param right right node, can't be null
     * @return true, if left node is the leader
     */
    public static boolean isLeftNodeLeader(NodeInfo left, NodeInfo right) {
        Objects.requireNonNull(right, "right");
        if (left == null) {
            return false;
        }
        return left.compareTo(right) < 0;
    }
}
