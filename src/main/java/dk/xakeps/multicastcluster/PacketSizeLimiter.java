package dk.xakeps.multicastcluster;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelOutboundHandlerAdapter;
import io.netty.channel.ChannelPromise;

/**
 * Limits packet size
 */
public class PacketSizeLimiter extends ChannelOutboundHandlerAdapter {
    private final int maxSize;

    public PacketSizeLimiter(int maxSize) {
        this.maxSize = maxSize;
    }

    @Override
    public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception {
        if (((ByteBuf) msg).writerIndex() > maxSize) {
            throw new IllegalStateException("Packet exceeds maximum capacity");
        }
        super.write(ctx, msg, promise);
    }
}
