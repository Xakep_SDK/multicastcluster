package dk.xakeps.multicastcluster;

import dk.xakeps.multicastcluster.packets.Packet;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

import java.util.Objects;

/**
 * Netty pipeline packet encoder
 */
public class PacketEncoder extends MessageToByteEncoder<Packet> {
    private final PacketRegistry packetRegistry;

    public PacketEncoder(PacketRegistry packetRegistry) {
        this.packetRegistry = Objects.requireNonNull(packetRegistry, "packetRegistry");
    }

    @Override
    protected void encode(ChannelHandlerContext ctx, Packet packet, ByteBuf byteBuf) {
        packetRegistry.write(packet, byteBuf);
    }
}
