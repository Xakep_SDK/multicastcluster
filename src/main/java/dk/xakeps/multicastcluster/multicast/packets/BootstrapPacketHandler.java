package dk.xakeps.multicastcluster.multicast.packets;

import dk.xakeps.multicastcluster.Config;
import dk.xakeps.multicastcluster.NodeInfo;
import dk.xakeps.multicastcluster.multicast.MulticastHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.util.concurrent.GlobalEventExecutor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Objects;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;

/**
 * Handles initial startup
 * Finds the leader
 */
public class BootstrapPacketHandler implements MulticastPacketHandler {
    private static final Logger LOGGER = LogManager.getLogger(BootstrapPacketHandler.class);

    private final Config config;
    private final SortedSet<NodeInfo> votes = new TreeSet<>();
    private final MulticastHandler multicastHandler;

    public BootstrapPacketHandler(Config config, MulticastHandler multicastHandler) {
        this.config = Objects.requireNonNull(config, "config");
        this.multicastHandler = Objects.requireNonNull(multicastHandler, "multicastHandler");
    }

    /**
     * Somebody started voting, vote too
     * @param ctx context
     * @param packet vote request packet
     */
    @Override
    public void handle(ChannelHandlerContext ctx, VoteRequestPacket packet) {
        LOGGER.debug("Vote request received: {}", packet);
        ctx.writeAndFlush(config.getVoteResponsePacket());
    }

    /**
     * Leader sent his information, account it
     * @param ctx context
     * @param packet leader info packet
     */
    @Override
    public void handle(ChannelHandlerContext ctx, LeaderInfoPacket packet) {
        LOGGER.debug("Leader response received: {}", packet);
        this.votes.add(packet);
    }

    /**
     * Somebody sent vote response, write it
     * @param ctx context
     * @param packet vote response packet
     */
    public void handle(ChannelHandlerContext ctx, VoteResponsePacket packet) {
        LOGGER.debug("Vote response counted: {}", packet);
        votes.add(packet);
    }

    @Override
    public void onConnect(ChannelHandlerContext ctx) {
        ctx.writeAndFlush(config.getVoteRequestPacket());

        // todo: better deciding system
        // add voting id to each vote packet
        // increment wait time by some margin when new voting packet with same voting id received
        GlobalEventExecutor.INSTANCE.schedule(() -> {
            LOGGER.trace("Leader check started");
            NodeInfo selectedLeader = votes.first();

            LOGGER.trace("I am leader: {}, Leader is: {}",
                    selectedLeader.address().equals(config.getTcpServerAddress()),
                    selectedLeader.address());
            if (config.getTcpServerAddress().equals(selectedLeader.address())) {
                // todo: better launch new server here
                ServerModePacketHandler handler = new ServerModePacketHandler(config, multicastHandler);
                multicastHandler.setPacketHandler(handler);
                handler.onConnect(ctx);
            } else {
                ClientModePacketHandler handler = new ClientModePacketHandler(config, selectedLeader, multicastHandler);
                multicastHandler.setPacketHandler(handler);
                handler.onConnect(ctx);
            }
        }, config.getInitialWaitTime().toMillis(), TimeUnit.MILLISECONDS);
    }
}
