package dk.xakeps.multicastcluster.multicast.packets;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;

/**
 * This packet is sent to find a leader or to take part in voting
 * If leader is present, {@link LeaderInfoPacket} will be sent to this node
 * or {@link VoteResponsePacket}
 */
public record VoteRequestPacket(long timestamp) implements MulticastPacket {
    @Override
    public void handle(ChannelHandlerContext ctx, MulticastPacketHandler packetHandler) {
        packetHandler.handle(ctx, this);
    }

    @Override
    public void write(ByteBuf buf) {
        buf.writeLong(timestamp);
    }
}
