package dk.xakeps.multicastcluster.multicast.packets;

import dk.xakeps.multicastcluster.NodeInfo;
import dk.xakeps.multicastcluster.packets.Packet;
import dk.xakeps.multicastcluster.packets.PacketDeserializer;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;

/**
 * This packet is sent by the leader when node sends {@link VoteRequestPacket}
 */
public record LeaderInfoPacket(long timestamp, InetSocketAddress address)
        implements MulticastPacket, NodeInfo {
    @Override
    public void handle(ChannelHandlerContext ctx, MulticastPacketHandler packetHandler) {
        packetHandler.handle(ctx, this);
    }

    @Override
    public void write(ByteBuf buf) {
        byte[] address = this.address.getAddress().getAddress();
        int length = address.length;
        buf.writeByte(length);
        buf.writeBytes(address);
        buf.writeShort(this.address.getPort());
        buf.writeLong(timestamp);
    }

    public static class Deserializer implements PacketDeserializer {
        @Override
        public Packet decode(ByteBuf buf) {
            byte b = buf.readByte();
            byte[] addr = new byte[b];
            buf.readBytes(addr);
            InetAddress byAddress;
            try {
                byAddress = InetAddress.getByAddress(addr);
            } catch (UnknownHostException e) {
                throw new RuntimeException("Can't deserialize", e);
            }
            int port = buf.readUnsignedShort();
            long timestamp = buf.readLong();
            return new LeaderInfoPacket(timestamp, new InetSocketAddress(byAddress, port));
        }
    }
}
