package dk.xakeps.multicastcluster.multicast.packets;

import dk.xakeps.multicastcluster.PacketHandler;
import io.netty.channel.ChannelHandlerContext;

/**
 * Handles multicast packets
 */
public interface MulticastPacketHandler extends PacketHandler {
    /**
     * Called when vote request packet received
     * @param ctx context
     * @param packet vote request packet
     */
    void handle(ChannelHandlerContext ctx, VoteRequestPacket packet);

    /**
     * Called when leader info packet received
     * @param ctx context
     * @param packet leader info packet
     */
    void handle(ChannelHandlerContext ctx, LeaderInfoPacket packet);

    /**
     * Called when vote response packet received
     * @param ctx context
     * @param packet vote response packet
     */
    void handle(ChannelHandlerContext ctx, VoteResponsePacket packet);
}
