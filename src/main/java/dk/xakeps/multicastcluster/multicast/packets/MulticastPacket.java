package dk.xakeps.multicastcluster.multicast.packets;

import dk.xakeps.multicastcluster.PacketHandler;
import dk.xakeps.multicastcluster.packets.Packet;
import io.netty.channel.ChannelHandlerContext;

/**
 * Represents UDP packet
 */
public interface MulticastPacket extends Packet {
    @Override
    default void handle(ChannelHandlerContext ctx, PacketHandler packetHandler) {
        handle(ctx, (MulticastPacketHandler) packetHandler);
    }

    void handle(ChannelHandlerContext ctx, MulticastPacketHandler packetHandler);
}
