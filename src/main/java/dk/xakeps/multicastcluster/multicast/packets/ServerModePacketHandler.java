package dk.xakeps.multicastcluster.multicast.packets;

import dk.xakeps.multicastcluster.Config;
import dk.xakeps.multicastcluster.Utils;
import dk.xakeps.multicastcluster.multicast.MulticastHandler;
import io.netty.channel.ChannelHandlerContext;

/**
 * If this node acts as a leader, this handler manages this state
 */
public class ServerModePacketHandler implements MulticastPacketHandler {
    private final Config config;
    private final MulticastHandler multicastHandler;

    public ServerModePacketHandler(Config config,
                                   MulticastHandler multicastHandler) {
        this.config = config;
        this.multicastHandler = multicastHandler;
    }

    @Override
    public void onConnect(ChannelHandlerContext ctx) {
        // tell everybody - I am the leader
        config.getClientManager().setAcceptClients(true);
        ctx.writeAndFlush(config.getLeaderInfoPacket());
    }

    @Override
    public void handle(ChannelHandlerContext ctx, VoteRequestPacket packet) {
        // somebody requested leader info, send myself
        ctx.writeAndFlush(config.getLeaderInfoPacket());
    }

    @Override
    public void handle(ChannelHandlerContext ctx, LeaderInfoPacket packet) {
        if (config.getLeaderInfoPacket().equals(packet)) {
            // our own packet, ignore
            return;
        }

        if (Utils.isLeftNodeLeader(config.getLeaderInfoPacket(), packet)) {
            // I am the leader, it's ok, ignore
            return;
        }

        // I act as a leader, but I shouldn't be one, disconnect myself
        BootstrapPacketHandler handler = new BootstrapPacketHandler(config, multicastHandler);
        multicastHandler.setPacketHandler(handler);
        config.getClientManager().setAcceptClients(false);
        config.getClientManager().disconnectAll();
        handler.onConnect(ctx);
    }

    @Override
    public void handle(ChannelHandlerContext ctx, VoteResponsePacket packet) {

    }
}
