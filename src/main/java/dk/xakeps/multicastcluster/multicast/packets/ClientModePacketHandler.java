package dk.xakeps.multicastcluster.multicast.packets;

import dk.xakeps.multicastcluster.Config;
import dk.xakeps.multicastcluster.NodeInfo;
import dk.xakeps.multicastcluster.Utils;
import dk.xakeps.multicastcluster.multicast.MulticastHandler;
import dk.xakeps.multicastcluster.tcp.TcpClient;
import io.netty.channel.ChannelHandlerContext;

import java.util.Objects;

/**
 * Handles packets when this node is a follower
 */
public class ClientModePacketHandler implements MulticastPacketHandler {
    private final Config config;

    private final TcpClient tcpClient;
    private final MulticastHandler multicastHandler;
    private volatile NodeInfo currentServer;

    public ClientModePacketHandler(Config config,
                                   NodeInfo currentServer,
                                   MulticastHandler multicastHandler) {
        this.config = Objects.requireNonNull(config, "config");
        this.currentServer = Objects.requireNonNull(currentServer, "currentServer");
        this.multicastHandler = Objects.requireNonNull(multicastHandler, "multicastHandler");
        this.tcpClient = new TcpClient();
    }

    /**
     * Client should connect to the leader
     * @param ctx context
     */
    @Override
    public void onConnect(ChannelHandlerContext ctx) {
        this.tcpClient.connect(currentServer.address(), () -> onDisconnect(ctx));
    }

    @Override
    public void handle(ChannelHandlerContext ctx, VoteRequestPacket packet) {
        // should not care, because when client disconnects, BootstrapPacketHandler should be activated
        // and it should handle when this node is not connected anywhere
    }

    /**
     * Check if leader changed and reconnect if needed
     * @param ctx context
     * @param packet leader info packet
     */
    @Override
    public void handle(ChannelHandlerContext ctx, LeaderInfoPacket packet) {
        if (!Utils.isLeftNodeLeader(currentServer, packet)) {
            this.currentServer = packet;
            tcpClient.disconnect();
            tcpClient.connect(currentServer.address(), () -> onDisconnect(ctx));
        }
    }

    @Override
    public void handle(ChannelHandlerContext ctx, VoteResponsePacket packet) {
        // should not care, because when client disconnects, BootstrapPacketHandler should be activated
        // and it should handle when this node is not connected anywhere
    }

    /**
     * Server disconnected, find a new leader
     * @param ctx context
     */
    private void onDisconnect(ChannelHandlerContext ctx) {
        BootstrapPacketHandler handler = new BootstrapPacketHandler(config, multicastHandler);
        multicastHandler.setPacketHandler(handler);
        handler.onConnect(ctx);
    }
}
