package dk.xakeps.multicastcluster.multicast;

import dk.xakeps.multicastcluster.PacketRegistry;
import dk.xakeps.multicastcluster.multicast.packets.LeaderInfoPacket;
import dk.xakeps.multicastcluster.multicast.packets.VoteRequestPacket;
import dk.xakeps.multicastcluster.multicast.packets.VoteResponsePacket;

/**
 * Contains all UDO packets
 */
public class MulticastPacketRegistry extends PacketRegistry {
    public static final PacketRegistry INSTANCE;

    static {
        MulticastPacketRegistry registry = new MulticastPacketRegistry();

        registry.registerPacket(VoteRequestPacket.class, buf -> new VoteRequestPacket(buf.readLong()));
        registry.registerPacket(VoteResponsePacket.class, new VoteResponsePacket.Deserializer());
        registry.registerPacket(LeaderInfoPacket.class, new LeaderInfoPacket.Deserializer());

        INSTANCE = registry;
    }
}
