package dk.xakeps.multicastcluster.multicast;

import dk.xakeps.multicastcluster.Config;
import dk.xakeps.multicastcluster.multicast.packets.BootstrapPacketHandler;
import dk.xakeps.multicastcluster.multicast.packets.MulticastPacketHandler;
import dk.xakeps.multicastcluster.packets.Packet;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Objects;

/**
 * Handles multicast UDP packets
 */
public class MulticastHandler extends SimpleChannelInboundHandler<Packet> {
    private static final Logger LOGGER = LogManager.getLogger(MulticastHandler.class);

    private final Config config;
    private MulticastPacketHandler packetHandler;

    public MulticastHandler(Config config) {
        this.config = Objects.requireNonNull(config, "config");
        this.packetHandler = new BootstrapPacketHandler(config, this);
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) {
        packetHandler.onConnect(ctx);
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, Packet msg) {
        msg.handle(ctx, packetHandler);
    }

    public void setPacketHandler(MulticastPacketHandler packetHandler) {
        LOGGER.info("Packet handler activated: " + packetHandler.getClass().getSimpleName());
        this.packetHandler = packetHandler;
    }
}
