package dk.xakeps.multicastcluster.multicast;

import dk.xakeps.multicastcluster.Config;
import dk.xakeps.multicastcluster.PacketDecoder;
import dk.xakeps.multicastcluster.PacketEncoder;
import dk.xakeps.multicastcluster.PacketSizeLimiter;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.DatagramChannel;
import io.netty.channel.socket.nio.NioDatagramChannel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Objects;

/**
 * MulticastServer is a UDP multicast server
 * It finds other nodes in the network and coordinates network leader
 */
public class MulticastServer {
    private static final Logger LOGGER = LogManager.getLogger(MulticastServer.class);

    private final Config config;
    private final Bootstrap bootstrap;
    private DatagramChannel channel;

    public MulticastServer(Config config) {
        this.config = Objects.requireNonNull(config, "config");
        this.bootstrap = createBootstrap(config);
    }

    /**
     * Starts this server
     * @throws Exception if something is wrong
     */
    public void start() throws Exception {
        LOGGER.info("Multicast server stared. Address: {}", config.getGroupAddress());
        ChannelFuture sync = bootstrap.bind(config.getGroupAddress().getPort()).sync();
        this.channel = (DatagramChannel) sync.channel();
        channel.joinGroup(config.getGroupAddress(), config.getMulticastInterface()).sync();
    }

    /**
     * Stops this server if it was started
     */
    public void stop() throws InterruptedException {
        if (this.channel == null) {
            return;
        }
        LOGGER.info("Multicast server stopped");
        this.channel.close().sync();
        this.channel = null;
    }

    private static Bootstrap createBootstrap(Config config) {
        EventLoopGroup bossGroup = new NioEventLoopGroup();
        return new Bootstrap()
                .group(bossGroup)
                .channel(NioDatagramChannel.class)
                .handler(new ChannelInitializer<NioDatagramChannel>() {
                    @Override
                    protected void initChannel(NioDatagramChannel ch) {
                        ChannelPipeline pipeline = ch.pipeline();
                        pipeline
                                .addLast(new DatagramDecoder())
                                .addLast(new PacketDecoder(MulticastPacketRegistry.INSTANCE))

                                .addLast(new DatagramEncoder(config.getGroupAddress()))
                                .addLast(new PacketSizeLimiter(28)) // packets must be small, 28 bytes is a maximum size
                                .addLast(new PacketEncoder(MulticastPacketRegistry.INSTANCE))

                                .addLast(new MulticastHandler(config));
                    }
                })
                .option(ChannelOption.IP_MULTICAST_IF, config.getMulticastInterface())
                .localAddress(config.getTcpServerAddress())
                .option(ChannelOption.SO_REUSEADDR, true);
    }
}