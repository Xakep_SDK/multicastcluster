package dk.xakeps.multicastcluster.multicast;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.socket.DatagramPacket;
import io.netty.handler.codec.MessageToMessageEncoder;

import java.net.InetSocketAddress;
import java.util.List;

/**
 * Encodes UDP packets
 */
public class DatagramEncoder extends MessageToMessageEncoder<ByteBuf> {
    private final InetSocketAddress groupAddr;

    public DatagramEncoder(InetSocketAddress groupAddr) {
        this.groupAddr = groupAddr;
    }

    @Override
    protected void encode(ChannelHandlerContext ctx, ByteBuf msg, List<Object> out) {
        out.add(new DatagramPacket(msg.retain(), groupAddr));
    }
}
