package dk.xakeps.multicastcluster;

import dk.xakeps.multicastcluster.packets.Packet;
import dk.xakeps.multicastcluster.tcp.TcpPacketHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

import java.util.Objects;

/**
 * Handles TCP connections
 */
public class TcpHandler extends SimpleChannelInboundHandler<Packet> {
    private final TcpPacketHandler packetHandler;

    public TcpHandler(TcpPacketHandler packetHandler) {
        this.packetHandler = Objects.requireNonNull(packetHandler, "packetHandler");
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) {
        packetHandler.onConnect(ctx);
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) {
        packetHandler.onDisconnect(ctx);
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, Packet msg) {
        msg.handle(ctx, packetHandler);
    }
}
