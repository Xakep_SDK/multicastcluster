package dk.xakeps.multicastcluster;

import io.netty.channel.ChannelHandlerContext;

/**
 * Very low lever interface for packet handling
 */
public interface PacketHandler {
    /**
     * Called when somebody connected or we connected somewhere
     * or handler should be activated
     * @param ctx context
     */
    void onConnect(ChannelHandlerContext ctx);
}
