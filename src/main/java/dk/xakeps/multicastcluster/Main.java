package dk.xakeps.multicastcluster;

import dk.xakeps.multicastcluster.multicast.MulticastServer;
import dk.xakeps.multicastcluster.tcp.TcpServer;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.Properties;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Application entry point
 */
public class Main {
    public static void main(String[] args) throws Exception {

        Properties properties = new Properties();
        Path path = Paths.get("settings.properties");
        if (Files.exists(path)) {
            try (BufferedReader reader = Files.newBufferedReader(path, StandardCharsets.UTF_8)) {
                properties.load(reader);
            }
        }

        String strIp = getProperty(properties, "ip", "127.0.0.1");
        String strPort = properties.getProperty("port", String.valueOf(
                ThreadLocalRandom.current().nextInt(1025, 65536)));

        String strGroupIp = getProperty(properties, "groupIp", "239.0.0.0");
        String strGroupPort = getProperty(properties, "groupPort", "1234");

        if (Files.notExists(path)) {
            try (BufferedWriter writer = Files.newBufferedWriter(path, StandardCharsets.UTF_8)) {
                properties.store(writer, """
                        ip - this IP will be used if this node becomes a leader
                        port - this port will be used if this node becomes a leader, don't set it for a random port
                        groupIp - multicast group IP, each cluster should decide it's group ip before it's started
                        groupPort - multicast group port, each cluster should decide it's group port before it's started
                        """);
            }
        }

        InetAddress tcpAddress = InetAddress.getByName(strIp);
        int tcpPort = Integer.parseInt(strPort);
        InetSocketAddress tcpServerAddress = new InetSocketAddress(tcpAddress, tcpPort);

        InetAddress groupAddr = InetAddress.getByName(strGroupIp);
        int groupPort = Integer.parseInt(strGroupPort);
        InetSocketAddress groupSocketAddress = new InetSocketAddress(groupAddr, groupPort);

        NetworkInterface multicastInterface = NetworkInterface.getByInetAddress(tcpServerAddress.getAddress());

        ClientManager clientManager = new ClientManager();
        Duration pingRate = Duration.ofSeconds(5);
        Duration initialWaitTime = Duration.ofSeconds(5);

        Config config = new Config(
                pingRate,
                initialWaitTime,
                System.currentTimeMillis(),
                tcpServerAddress,
                groupSocketAddress,
                multicastInterface,
                clientManager
        );

        Node node = new Node(config);
        node.start();
    }

    private static String getProperty(Properties properties, String key, String def) {
        String property = properties.getProperty(key);
        if (property != null) {
            return property;
        }
        properties.put(key, def);
        return def;
    }
}
