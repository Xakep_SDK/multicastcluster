package dk.xakeps.multicastcluster.packets;

import io.netty.buffer.ByteBuf;

/**
 * Packet deserializer
 */
public interface PacketDeserializer {
    /**
     * Decodes a packet
     * @param buf buffer from where to read a packet
     * @return packet that was read
     */
    Packet decode(ByteBuf buf);
}
