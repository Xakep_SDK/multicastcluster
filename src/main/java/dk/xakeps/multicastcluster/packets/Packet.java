package dk.xakeps.multicastcluster.packets;

import dk.xakeps.multicastcluster.PacketHandler;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;

public interface Packet {
    void handle(ChannelHandlerContext ctx, PacketHandler packetHandler);

    void write(ByteBuf buf);
}
