package dk.xakeps.multicastcluster;

import dk.xakeps.multicastcluster.packets.Packet;
import dk.xakeps.multicastcluster.packets.PacketDeserializer;
import io.netty.buffer.ByteBuf;

import java.util.HashMap;
import java.util.Map;

/**
 * Generic registry for packets
 */
public class PacketRegistry {
    private final Map<Byte, PacketDeserializer> decoders = new HashMap<>();
    private final Map<Class<? extends Packet>, Byte> packetIds = new HashMap<>();

    private byte packetId = Byte.MIN_VALUE;

    protected PacketRegistry() {
    }

    /**
     * Registers new packet
     * @param messageClass packet class
     * @param packetDeserializer packet deserializer
     */
    protected void registerPacket(Class<? extends Packet> messageClass, PacketDeserializer packetDeserializer) {
        byte oldPacketId = packetId;
        byte newPacketId = packetId++;
        if (oldPacketId > newPacketId) {
            packetId = oldPacketId;
            throw new IllegalStateException("Maximum number of packets exceeded");
        }

        decoders.put(newPacketId, packetDeserializer);
        packetIds.put(messageClass, newPacketId);
    }

    /**
     * Reads new packet
     * @param buf stream data
     * @return packet
     */
    public Packet read(ByteBuf buf) {
        byte i = buf.readByte();
        PacketDeserializer packetDeserializer = decoders.get(i);
        if (packetDeserializer == null) {
            throw new IllegalArgumentException("Wrong packet id: " + i);
        }

        return packetDeserializer.decode(buf);
    }

    /**
     * Writes packet
     * @param packet packet to write
     * @param buf buffer where packet will be written
     */
    public void write(Packet packet, ByteBuf buf) {
        Byte packetId = packetIds.get(packet.getClass());
        if (packetId == null) {
            throw new IllegalArgumentException("Packet not registered: " + packet.getClass());
        }

        buf.writeByte(packetId);
        packet.write(buf);
    }
}
